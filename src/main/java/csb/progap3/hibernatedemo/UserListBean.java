package csb.progap3.hibernatedemo;

import java.util.List;

import javax.faces.bean.ManagedBean;

import csb.progap3.hibernatedemo.businesslogic.UserService;
import csb.progap3.hibernatedemo.dao.UserDao;
import csb.progap3.hibernatedemo.models.User;


@ManagedBean
public class UserListBean {

	private UserService users;
	private List<User> userList;

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	
	public UserListBean() {
		users = new UserDao();
		userList = users.listUsers();
	}
	
}
