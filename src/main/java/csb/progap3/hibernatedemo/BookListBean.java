package csb.progap3.hibernatedemo;

import java.util.List;

import javax.faces.bean.ManagedBean;

import csb.progap3.hibernatedemo.businesslogic.BookService;
import csb.progap3.hibernatedemo.dao.BookDao;
import csb.progap3.hibernatedemo.models.Book;


@ManagedBean
public class BookListBean {

	private BookService books;
	private List<Book> bookList;

	public List<Book> getBookList() {
		return bookList;
	}

	public void setBookList(List<Book> bookList) {
		this.bookList = bookList;
	}
	
	public BookListBean() {
		books = new BookDao();
		bookList = books.listBooks();
	}
	
}
