package csb.progap3.hibernatedemo.dao;



import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import csb.progap3.hibernatedemo.businesslogic.AdminService;
import csb.progap3.hibernatedemo.models.Admin;

public class AdminDao implements AdminService {

	private EntityManagerFactory emf;
	
	public AdminDao() {
		emf = Persistence.createEntityManagerFactory("FranzDB");
	}

	


	@Override
	public Admin loginAdmin(String username, String password){
		EntityManager em = emf.createEntityManager();
		try{
		em.getTransaction().begin();
		Admin admin=
		em.createQuery("select e from Admin e where e.username = :username AND e.password = :password", Admin.class)
		.setParameter("username", username)
		.setParameter("password", password)
		.getSingleResult();
		em.getTransaction().commit();
		em.close();
		return admin;
		}
		catch(Exception e){
		e.printStackTrace();
		em.getTransaction().rollback();
		em.close();
		return null;
		}}




	@Override
	public boolean createAdmin(Admin admin) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(admin);
			em.getTransaction().commit();
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			
		return false;
	}}}

	