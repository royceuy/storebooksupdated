package csb.progap3.hibernatedemo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import csb.progap3.hibernatedemo.businesslogic.EmployeeService;
import csb.progap3.hibernatedemo.models.Employee;

public class EmployeeDao implements EmployeeService {

	private EntityManagerFactory emf;
	
	public EmployeeDao() {
		emf = Persistence.createEntityManagerFactory("FranzDB");
	}

	@Override
	public List<Employee> listEmployees() {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			List<Employee> employees = 
					em.createQuery("from Employee", Employee.class).getResultList();
			em.getTransaction().commit();
			em.close();
			return employees;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return null;
		}
	}

	@Override
	public Employee findEmployee(String employeeId) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Employee employee = 
					em.createQuery("select e from Employee e where e.employeeId = :employee_id", Employee.class)
						.setParameter("employee_id", employeeId)
						.getSingleResult();
			em.getTransaction().commit();
			em.close();
			return employee;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return null;
		}
	}

	@Override
	public boolean createEmployee(Employee employee) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(employee);
			em.getTransaction().commit();
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return false;
		}
	}

	@Override
	public boolean updateEmployee(String employeeId, Employee updatedEmployee) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Employee employee = 
					em.createQuery("select e from Employee e where e.employeeId = :employee_id", Employee.class)
						.setParameter("employee_id", employeeId)
						.getSingleResult();
			employee = em.merge(updatedEmployee);
			em.persist(employee);
			em.getTransaction().commit();
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return false;
		}
	}

	@Override
	public boolean removeEmployee(String employeeId) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Employee employee = 
					em.createQuery("select e from Employee e where e.employeeId = :employee_id", Employee.class)
						.setParameter("employee_id", employeeId)
						.getSingleResult();
			em.remove(employee);
			em.getTransaction().commit();
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return false;
		}
	}
}
