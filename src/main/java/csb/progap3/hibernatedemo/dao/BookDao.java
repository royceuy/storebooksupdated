package csb.progap3.hibernatedemo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import csb.progap3.hibernatedemo.businesslogic.BookService;
import csb.progap3.hibernatedemo.models.Book;

public class BookDao implements BookService {

	private EntityManagerFactory emf;
	
	public BookDao() {
		emf = Persistence.createEntityManagerFactory("FranzDB");
	}

	@Override
	public List<Book> listBooks() {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			List<Book> books = 
					em.createQuery("from Book", Book.class).getResultList();
			em.getTransaction().commit();
			em.close();
			return books;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return null;
		}
	}

	@Override
	public Book findBook(String bookId) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Book book = 
					em.createQuery("select b from Book b where b.bookId = :book_id", Book.class)
						.setParameter("book_id", bookId)
						.getSingleResult();
			em.getTransaction().commit();
			em.close();
			return book;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return null;
		}
	}

	@Override
	public boolean createBook(Book book) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(book);
			em.getTransaction().commit();
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return false;
		}
	}

	@Override
	public boolean updateBook(String bookId, Book updatedBook) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Book book = 
					em.createQuery("select b from Book b where b.bookId = :book_id", Book.class)
						.setParameter("book_id", bookId)
						.getSingleResult();
			book = em.merge(updatedBook);
			em.persist(book);
			em.getTransaction().commit();
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return false;
		}
	}

	@Override
	public boolean removeBook(String bookId) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Book book = 
					em.createQuery("select b from Book b where b.bookId = :book_id", Book.class)
						.setParameter("book_id", bookId)
						.getSingleResult();
			em.remove(book);
			em.getTransaction().commit();
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return false;
		}
	}
}
