package csb.progap3.hibernatedemo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import csb.progap3.hibernatedemo.businesslogic.UserService;
import csb.progap3.hibernatedemo.models.User;

public class UserDao implements UserService {

	private EntityManagerFactory emf;
	
	public UserDao() {
		emf = Persistence.createEntityManagerFactory("FranzDB");
	}

	@Override
	public List<User> listUsers() {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			List<User> users = 
					em.createQuery("from User", User.class).getResultList();
			em.getTransaction().commit();
			em.close();
			return users;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return null;
		}
	}

	@Override
	public User findUser(String userId) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			User user = 
					em.createQuery("select u from User u where u.userId = :user_id", User.class)
						.setParameter("user_id", userId)
						.getSingleResult();
			em.getTransaction().commit();
			em.close();
			return user;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return null;
		}
	}

	@Override
	public boolean createUser(User user) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(user);
			em.getTransaction().commit();
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return false;
		}
	}

	@Override
	public boolean updateUser(String userId, User updatedUser) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			User user = 
					em.createQuery("select u from User u where u.userId = :user_id", User.class)
						.setParameter("user_id", userId)
						.getSingleResult();
			user = em.merge(updatedUser);
			em.persist(user);
			em.getTransaction().commit();
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return false;
		}
	}

	@Override
	public boolean removeUser(String userId) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			User user = 
					em.createQuery("select u from User u where u.userId = :user_id", User.class)
						.setParameter("user_id", userId)
						.getSingleResult();
			em.remove(user);
			em.getTransaction().commit();
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			em.close();
			return false;
		}
	}
	
	
}
