package csb.progap3.hibernatedemo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Admin {

	private String adminId;
	
	private String userName;
	
	private String password;
	
	@Id
	@Column
	public String getAdminId() {
		return adminId;
	}

	
	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}
	

	@Column(nullable=false, length=20)
	public String getuserName() {
		return userName;
	}
	
	public void setuserName(String userName) {
		this.userName = userName;
	}
	

	@Column(nullable=false, length=20)
	public String getpassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	 
}
