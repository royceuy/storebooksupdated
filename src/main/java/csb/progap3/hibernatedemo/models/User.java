package csb.progap3.hibernatedemo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {

	private String userId;
	
	private String lastName;
	
	private String firstName;
	
	private String email;
	
	private String password;
	
	@Id
	@Column(nullable=false, length=10)
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Column(nullable=false, length=50)
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(nullable=false, length=50)
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	

	@Column(nullable=false, length=50)
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	

	@Column(nullable=false, length=50)
	public String getpassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
}
