package csb.progap3.hibernatedemo;

import javax.faces.bean.ManagedBean;

import csb.progap3.hibernatedemo.businesslogic.BookService;
import csb.progap3.hibernatedemo.dao.BookDao;
import csb.progap3.hibernatedemo.models.Book;

@ManagedBean
public class BooksBean {

	private BookService books;
	private Book book;
	private String[] genres = {"Romance", "Mystery", "Adventure", "History", "Sci-Fi"};

	public BooksBean() {
		books = new BookDao();
		book = new Book();
	}
	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}
	
	
	
	public String[] getGenres() {
		return genres;
	}
	public String add() {
		return "add-books";
	}

	public String create() {
		books.createBook(this.book);
		return "index3";
	}
	
	public String edit(String bookId) {
		this.book = books.findBook(bookId);
		return "edit-books";
	}
	
	public String update() {
		books.updateBook(this.book.getBookId(), this.book);
		return "index3";
	}
	
	public String delete(String bookId) {
		this.book = books.findBook(bookId);
		return "delete-books";
	}
	
	public String remove() {
		books.removeBook(this.book.getBookId());
		return "index3";
	}
	
	
	
	public String view(String bookId) {
		this.book = books.findBook(bookId);
		return "user-viewbooks";
	}
	
	
}
