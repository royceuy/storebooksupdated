package csb.progap3.hibernatedemo;

import javax.faces.bean.ManagedBean;

import csb.progap3.hibernatedemo.businesslogic.EmployeeService;
import csb.progap3.hibernatedemo.dao.EmployeeDao;
import csb.progap3.hibernatedemo.models.Employee;

@ManagedBean
public class EmployeeBean {

	private EmployeeService emps;
	private Employee employee;

	public EmployeeBean() {
		emps = new EmployeeDao();
		employee = new Employee();
	}
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	public String add() {
		return "add-employee";
	}

	public String create() {
		emps.createEmployee(this.employee);
		return "index";
	}
	
	public String edit(String employeeId) {
		this.employee = emps.findEmployee(employeeId);
		return "edit-employee";
	}
	
	public String update() {
		emps.updateEmployee(this.employee.getEmployeeId(), this.employee);
		return "index";
	}
	
	public String delete(String employeeId) {
		this.employee = emps.findEmployee(employeeId);
		return "delete-employee";
	}
	
	public String remove() {
		emps.removeEmployee(this.employee.getEmployeeId());
		return "index";
	}
}
