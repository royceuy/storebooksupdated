package csb.progap3.hibernatedemo;

import javax.faces.bean.ManagedBean;

import csb.progap3.hibernatedemo.businesslogic.AdminService;
import csb.progap3.hibernatedemo.dao.AdminDao;
import csb.progap3.hibernatedemo.models.Admin;

@ManagedBean
public class AdminBean {
	

	private AdminService admins;
	private Admin admin;
	
	public AdminBean() {
		admins = new AdminDao();
		admin = new Admin();
	}
	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
	

	public String add() {
		return "add-admins";
	}

	public String create() {
		admins.createAdmin(this.admin);
		return "AdminLogin";
	}
	
	
	
	
}
