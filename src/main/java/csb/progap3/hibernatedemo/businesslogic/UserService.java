package csb.progap3.hibernatedemo.businesslogic;

import java.util.List;

import csb.progap3.hibernatedemo.models.User;

public interface UserService {
	public List<User> listUsers();
	public User findUser(String userId);
	public boolean createUser(User user);
	public boolean updateUser(String userId, User user);
	public boolean removeUser(String userId);
}


