package csb.progap3.hibernatedemo.businesslogic;

import java.util.List;

import csb.progap3.hibernatedemo.models.Book;

public interface BookService {
	public List<Book> listBooks();
	public Book findBook(String bookId);
	public boolean createBook(Book book);
	public boolean updateBook(String bookId, Book book);
	public boolean removeBook(String bookId);
}


