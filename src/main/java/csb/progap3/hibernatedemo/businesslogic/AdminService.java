package csb.progap3.hibernatedemo.businesslogic;

import csb.progap3.hibernatedemo.models.Admin;



public interface AdminService {
	
	public Admin loginAdmin(String username, String password);
	public boolean createAdmin(Admin admin);
}


