package csb.progap3.hibernatedemo.businesslogic;

import java.util.List;

import csb.progap3.hibernatedemo.models.Employee;

public interface EmployeeService {
	public List<Employee> listEmployees();
	public Employee findEmployee(String employeeId);
	public boolean createEmployee(Employee employee);
	public boolean updateEmployee(String employeeId, Employee employee);
	public boolean removeEmployee(String employeeId);
}


