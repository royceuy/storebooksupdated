package csb.progap3.hibernatedemo;

import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;

import csb.progap3.hibernatedemo.businesslogic.UserService;
import csb.progap3.hibernatedemo.dao.UserDao;
import csb.progap3.hibernatedemo.models.User;

@ManagedBean
public class UserBean {

	private UserService users;
	private User user;

	public UserBean() {
		users = new UserDao();
		user = new User();
	}
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public String add() {
		return "add-user";
	}

	public String create() {
		users.createUser(this.user);
		return "index2";
	}
	
	public String edit(String userId) {
		this.user = users.findUser(userId);
		return "edit-user";
	}
	
	public String update() {
		users.updateUser(this.user.getUserId(), this.user);
		return "index2";
	}
	
	public String delete(String userId) {
		this.user = users.findUser(userId);
		return "delete-user";
	}
	
	public String remove() {
		users.removeUser(this.user.getUserId());
		return "index2";
	}
	
}
