package csb.progap3.hibernatedemo;

import java.util.List;

import javax.faces.bean.ManagedBean;

import csb.progap3.hibernatedemo.businesslogic.EmployeeService;
import csb.progap3.hibernatedemo.dao.EmployeeDao;
import csb.progap3.hibernatedemo.models.Employee;


@ManagedBean
public class EmployeeListBean {

	private EmployeeService emps;
	private List<Employee> employeeList;

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}
	
	public EmployeeListBean() {
		emps = new EmployeeDao();
		employeeList = emps.listEmployees();
	}
	
}
